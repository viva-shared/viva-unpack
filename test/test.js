// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\test2.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\test2.zip\test1.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\test2.zip\test1.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\test2.zip\test1.zip\2.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\test2.zip\Empty_excel.xlsx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1\test2.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\2\
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\2\3.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\2\2.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip\test1.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip\Empty_excel.xlsx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip\test1.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip\test1.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test3.zip\1\test2.zip\test1.zip\2.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test2.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test2.zip\test1.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test2.zip\test1.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test2.zip\test1.zip\2.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test2.zip\Empty_excel.xlsx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test2.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test1.zip
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test1.zip\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\test1.zip\2.txt
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\Empty_excel.xlsx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\empty_doc.docx
// d:\Work\gitlab\viva-shared\viva-unpack\test\testzip.zip\test\1.txt

// @ts-check
const assert = require('assert');
const testing_lib = require('./../index.js')
let lib_fs = require('fs')
let lib_path = require('path')
let lib_vconv = require('viva-convert')

let deleteFolderRecursive = function(path) {
    if (lib_fs.existsSync(path)) {
        lib_fs.readdirSync(path).forEach(function(file, index) {
            let curPath = path + "/" + file
            if (lib_fs.lstatSync(curPath).isDirectory()) {
                deleteFolderRecursive(curPath)
            } else {
                lib_fs.unlinkSync(curPath)
            }
        })
    lib_fs.rmdirSync(path)
    }
}

let path_root = lib_path.join(__dirname, 'root')
let path_file_zip = lib_path.join(__dirname, 'testzip.zip')
let path_file_nozip = lib_path.join(__dirname, 'testnozip.1')

let files_all = [
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\xl\\sharedStrings.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\xl\\styles.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\xl\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\xl\\workbook.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\xl\\worksheets\\sheet1.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\xl\\_rels\\workbook.xml.rels'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\Empty_excel.xlsx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\1.txt'),
    lib_path.join(path_root, 'test\\2\\2.txt'),
    lib_path.join(path_root, 'test\\2\\3.txt'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\xl\\sharedStrings.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\xl\\styles.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\xl\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\xl\\workbook.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\xl\\worksheets\\sheet1.xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\xl\\_rels\\workbook.xml.rels'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\Empty_excel.xlsx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\xl\\sharedStrings.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\xl\\styles.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\xl\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\xl\\workbook.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\xl\\worksheets\\sheet1.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\xl\\_rels\\workbook.xml.rels'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test2.zip\\Empty_excel.xlsx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\xl\\sharedStrings.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\xl\\styles.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\xl\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\xl\\workbook.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\xl\\worksheets\\sheet1.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\xl\\_rels\\workbook.xml.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\Empty_excel.xlsx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\docProps\\app.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\docProps\\core.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\document2.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\fontTable.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\settings.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\styles.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\theme\\theme1.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\webSettings.xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\word\\_rels\\document2.xml.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\[Content_Types].xml'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx\\_rels\\.rels'),
    lib_path.join(path_root, 'test\\test3.zip\\1.txt')
]

let files_no_all = [
    lib_path.join(path_root, 'test\\1.txt'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\empty_excel.xlsx'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\1\\test2.zip\\test1.zip\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\2\\2.txt'),
    lib_path.join(path_root, 'test\\2\\3.txt'),
    lib_path.join(path_root, 'test\\empty_excel.xlsx'),
    lib_path.join(path_root, 'test\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\test1.zip\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_excel.xlsx'),
    lib_path.join(path_root, 'test\\test2.zip\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\test2.zip\\test1.zip\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\test3.zip\\1.txt'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_doc.docx'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\empty_excel.xlsx'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\2.txt'),
    lib_path.join(path_root, 'test\\test3.zip\\1\\test2.zip\\test1.zip\\empty_doc.docx')
]

describe("unpack", function() {
    it('no zip file', function() {
        deleteFolderRecursive(path_root)
        lib_fs.mkdirSync(path_root)
        assert.equal(testing_lib.unpack(path_file_nozip, path_root).join(), [].join())
    })

    it('zip file, unpacked all', function() {
        deleteFolderRecursive(path_root)
        lib_fs.mkdirSync(path_root)
        assert.equal(testing_lib.unpack(path_file_zip, path_root).map(m => { return lib_vconv.replaceAll(m.toLowerCase(),'\\','/') }).sort().join(), files_all.map(m => { return lib_vconv.replaceAll(m.toLowerCase(),'\\','/') }).sort().join())
    })

    it('zip file, unpacked with allow', function() {
        deleteFolderRecursive(path_root)
        lib_fs.mkdirSync(path_root)
        assert.equal(testing_lib.unpack(path_file_zip, path_root, {extensions: ['ZIP'], action_extension: 'allow'}).map(m => { return lib_vconv.replaceAll(m.toLowerCase(),'\\','/') }).sort().join(), files_no_all.map(m => { return lib_vconv.replaceAll(m.toLowerCase(),'\\','/') }).sort().join())
    })

    it('zip file, unpacked with deny', function() {
        deleteFolderRecursive(path_root)
        lib_fs.mkdirSync(path_root)
        assert.equal(testing_lib.unpack(path_file_zip, path_root, {extensions: ['docx','xlsx'], action_extension: 'deny'}).map(m => { return lib_vconv.replaceAll(m.toLowerCase(),'\\','/') }).sort().join(), files_no_all.map(m => { return lib_vconv.replaceAll(m.toLowerCase(),'\\','/') }).sort().join())
    })
})



// let res = testing_lib.unpack(path_file, path_root, {extensions: ['zip'], action_extension: 'deny'})
// console.log(res)
// let a = 5
