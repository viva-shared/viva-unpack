## Members

<dl>
<dt><a href="#viva-unpack_ (license MIT) recursive zip-archive unpacking made on the library adm-zip">viva-unpack: (license MIT) recursive zip-archive unpacking made on the library adm-zip</a></dt>
<dd></dd>
</dl>

## Functions

<dl>
<dt><a href="#unpack">unpack(path_file, path_root, [options])</a> ⇒ <code>Array.&lt;string&gt;</code></dt>
<dd></dd>
<dt><a href="#unpack_text">unpack_text(path_file, name_text_file)</a> ⇒ <code>string</code></dt>
<dd></dd>
<dt><a href="#pack_local_files">pack_local_files(paths_file_to_pack, path_pack)</a></dt>
<dd></dd>
<dt><a href="#pack_texts">pack_texts(paths_file_to_pack, path_pack)</a></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><a href="#type_options">type_options</a></dt>
<dd></dd>
<dt><a href="#type_text">type_text</a></dt>
<dd></dd>
</dl>

<a name="viva-unpack_ (license MIT) recursive zip-archive unpacking made on the library adm-zip"></a>

## viva-unpack: (license MIT) recursive zip-archive unpacking made on the library adm-zip
**Kind**: global variable  
**Author**: Vitalii Vasilev  
**License**: MIT  
<a name="unpack"></a>

## unpack(path_file, path_root, [options]) ⇒ <code>Array.&lt;string&gt;</code>
**Kind**: global function  
**Returns**: <code>Array.&lt;string&gt;</code> - all unpacked files  

| Param | Type | Description |
| --- | --- | --- |
| path_file | <code>string</code> | full file name |
| path_root | <code>string</code> | full root path name |
| [options] | [<code>type\_options</code>](#type_options) |  |

**Example**  
```js
require('viva-unpack').unpack('1.zip','C:\\unpack_to')
```
<a name="unpack_text"></a>

## unpack\_text(path_file, name_text_file) ⇒ <code>string</code>
**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| path_file | <code>string</code> | full file name |
| name_text_file | <code>string</code> | file name in archive |

**Example**  
```js
let text = require('viva-unpack').unpack_file('c:\\1.zip','1.txt')
```
<a name="pack_local_files"></a>

## pack\_local\_files(paths_file_to_pack, path_pack)
**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| paths_file_to_pack | <code>Array.&lt;string&gt;</code> \| <code>string</code> | full file name |
| path_pack | <code>string</code> | full root path name |

**Example**  
```js
require('viva-unpack').pack_local_files('c:\\1.txt','C:\\1.zip')
```
<a name="pack_local_files..files"></a>

### pack_local_files~files : <code>Array.&lt;string&gt;</code>
**Kind**: inner property of [<code>pack\_local\_files</code>](#pack_local_files)  
<a name="pack_texts"></a>

## pack\_texts(paths_file_to_pack, path_pack)
**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| paths_file_to_pack | [<code>Array.&lt;type\_text&gt;</code>](#type_text) \| [<code>type\_text</code>](#type_text) | full file name |
| path_pack | <code>string</code> | full root path name |

**Example**  
```js
require('viva-unpack').pack_local_files('c:\\1.txt','C:\\1.zip')
```
<a name="pack_texts..files"></a>

### pack_texts~files : [<code>Array.&lt;type\_text&gt;</code>](#type_text)
**Kind**: inner property of [<code>pack\_texts</code>](#pack_texts)  
<a name="type_options"></a>

## type\_options
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| extensions | <code>Array.&lt;string&gt;</code> | list file extension, example - ['docx','epub'] |
| action_extension | <code>string</code> | ignore extensions ('deny') or allow only this extension ('allow') |

<a name="type_text"></a>

## type\_text
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | body file |
| name | <code>string</code> | name file |

