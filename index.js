// @ts-check
/**
 * @name viva-unpack: (license MIT) recursive zip-archive unpacking made on the library adm-zip
 * @license MIT
 * @author Vitalii Vasilev
 */

/**
 * @typedef type_options
 * @property {string[]} extensions list file extension, example - ['docx','epub']
 * @property {string} action_extension ignore extensions ('deny') or allow only this extension ('allow')
 */

/**
 * @typedef type_text
 * @property {string} text body file
 * @property {string} name name file
 */

/**
 * @private
 * @typedef type_task
 * @property {string} file
 * @property {string} path
 * @property {boolean} done
 * @property {boolean} dir
 */


/**
 * @private
 * @typedef type_entries
 * @property {Object} initial
 * @property {boolean} isDirectory
 * @property {string} name
 */

let lib_vconv = require('viva-convert')
let lib_fs = require('fs-extra')
let lib_path = require('path')
var lib_zip = require('adm-zip')

exports.unpack=unpack
exports.unpack_text=unpack_text
exports.pack_local_files=pack_local_files
exports.pack_texts=pack_texts

/**
 * @static
 * @param {string} path_file full file name
 * @param {string} path_root full root path name
 * @param {type_options} [options]
 * @returns {string[]} all unpacked files
 * @example require('viva-unpack').unpack('1.zip','C:\\unpack_to')
 */
function unpack (path_file, path_root, options) {
    if (lib_vconv.isAbsent(options)) {
        options = {extensions: [], action_extension: ''}
    } else {
        if (lib_vconv.isAbsent(options.extensions) || !Array.isArray(options.extensions)) {
            options.extensions = []
        } else {
            options.extensions = options.extensions.filter(f => typeof f === 'string').map(m => { return lib_vconv.border_add(m.toLowerCase(),'.',undefined) })
        }
        if (lib_vconv.isEmpty(options.action_extension) || typeof options.action_extension !== 'string') {
            options.action_extension = ''
        }
    }

    /** @private @type {type_task[]} */
    let tasks = [{file: path_file, path: path_root, done: false, dir: false}]
    while (tasks.some(f => f.done === false)) {
        unpack_core(tasks, options)
    }
    return tasks.filter((f,index) => f.done === true && f.dir === false && index !== 0).map(m => { return m.file })
}

/**
 * @static
 * @private
 * @param {type_task[]} tasks
 * @param {type_options} [options]
 */
function unpack_core (tasks, options) {
    let task = tasks.find(f => f.done === false)
    let zip
    let zip_entries
    try {
        if (lib_vconv.isAbsent(task)) return
        task.done = true

        if (task.file === task.path) {
            if (options.action_extension === 'allow' && !options.extensions.includes(lib_path.extname(task.file).toLowerCase())) {
                return
            } if (options.action_extension === 'deny' && options.extensions.includes(lib_path.extname(task.file).toLowerCase())) {
                return
            }
        }

        zip = new lib_zip(task.file)
        zip_entries = convert_entries(zip.getEntries())

        if (task.file === task.path) {
            lib_fs.unlinkSync(task.file)
            lib_fs.mkdirSync(task.path)
            task.dir = true
        }

        zip_entries.filter(f => f.isDirectory === true).forEach(function(entry) {
            lib_fs.ensureDirSync(lib_path.join(task.path, entry.name))
        })

        zip_entries.filter(f => f.isDirectory === false).forEach(function(entry) {
            let file_path = lib_path.join(task.path, entry.name)
            if (lib_fs.existsSync(file_path)) {
                lib_fs.removeSync(file_path)
            }

            let file_name = lib_path.join(task.path, entry.name)
            lib_fs.ensureDirSync(lib_path.dirname(file_name))
            lib_fs.writeFileSync(file_name, entry.initial.getData())
            tasks.push({
                file: lib_path.join(task.path, entry.name),
                path: lib_path.join(task.path, entry.name),
                done: false,
                dir: false
            })
        })
    } catch (error) {
        let error_text = lib_vconv.toString(error,'').toLowerCase()
        if (!error_text.includes('invalid or unsupported zip format')) {
            throw error
        }
    } finally {
        zip_entries = undefined
        zip = undefined
    }
}

/**
 * @static
 * @param {string} path_file full file name
 * @param {string} name_text_file file name in archive
 * @returns {string}
 * @example let text = require('viva-unpack').unpack_file('c:\\1.zip','1.txt')
 */
function unpack_text (path_file, name_text_file) {
    let zip
    let zip_entries
    try {
        zip = new lib_zip(path_file)
        zip_entries = zip.getEntries()

        let fnd = zip_entries.find(f => f.isDirectory === false && lib_path.basename(f.entryName.toLowerCase()) === lib_path.basename(name_text_file).toLowerCase())
        if (!lib_vconv.isAbsent(fnd)) {
            return fnd.getData().toString('utf8')
        }

    } catch (error) {
        throw new Error(lib_vconv.toErrorMessage(error, 'with params path_file = "{0}", name_text_file = "{1}"',[path_file, name_text_file]))
    } finally {
        zip_entries = undefined
        zip = undefined
    }
}

/**
 * @static
 * @param {string[]|string} paths_file_to_pack full file name
 * @param {string} path_pack full root path name
 * @example require('viva-unpack').pack_local_files('c:\\1.txt','C:\\1.zip')
 */
function pack_local_files (paths_file_to_pack, path_pack) {
    /** @type {string[]} */
    let files = []
    if (!lib_vconv.isAbsent(paths_file_to_pack) && Array.isArray(paths_file_to_pack)) {
        files = paths_file_to_pack.map(m => { return lib_vconv.toString(m) }).filter(f => !lib_vconv.isEmpty(f))
    } else if (!lib_vconv.isEmpty(paths_file_to_pack)) {
        let file = lib_vconv.toString(paths_file_to_pack)
        if (!lib_vconv.isEmpty(file)) {
            files.push(file)
        }
    }
    if (files.length <= 0) {
        throw new Error ('empty list of files for archiving')
    }

    var zip = new lib_zip()
    try {
        files.forEach(file => {
            zip.addLocalFile(file)
        })
        zip.writeZip(path_pack)
    } catch (error) {
        throw error
    } finally {
        zip = undefined
    }
}

/**
 * @static
 * @param {type_text[]|type_text} paths_file_to_pack full file name
 * @param {string} path_pack full root path name
 * @example require('viva-unpack').pack_local_files('c:\\1.txt','C:\\1.zip')
 */
function pack_texts (paths_file_to_pack, path_pack) {
    /** @type {type_text[]} */
    let files = []
    if (!lib_vconv.isAbsent(paths_file_to_pack) && Array.isArray(paths_file_to_pack)) {
        files = paths_file_to_pack
    } else if (!lib_vconv.isAbsent(paths_file_to_pack)) {
        // @ts-ignore
        files.push(paths_file_to_pack)
    }
    if (files.length <= 0) {
        throw new Error ('empty list of text files for archiving')
    }
    if (files.some(f => lib_vconv.isEmpty(f.name) || lib_vconv.isEmpty(f.text))) {
        throw new Error ('exists items with empty text or name in list of text ')
    }

    var zip = new lib_zip()
    try {
        files.forEach(file => {
            zip.addFile(file.name, Buffer.from(file.text, 'utf8'))
        })
        zip.writeZip(path_pack)
    } catch (error) {
        throw error
    } finally {
        zip = undefined
    }
}

/**
 * @private
 * @static
 * @param {Object} entries full file name
 * @returns {type_entries[]}
 */
function convert_entries(entries) {
    /** @type {type_entries[]} */
    let result = []

    if (entries.some(f => f.entryName.includes('�'))) {
        let index = 1
        entries.filter(f => f.isDirectory === false).forEach(item => {
            let file_name = index.toString()
            let file_ext = lib_path.extname(item.entryName)
            if (!file_ext.includes('�')) {
                file_name = file_name.concat(file_ext)
            }
            result.push({
                initial: item,
                name: file_name,
                isDirectory: false
            })
            index++
        })
    } else {
        entries.forEach(item => {
            result.push({
                initial: item,
                name: item.entryName,
                isDirectory: item.isDirectory
            })
        })
    }
    return result
}